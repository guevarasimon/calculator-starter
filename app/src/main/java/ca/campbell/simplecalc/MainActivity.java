package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {
    EditText etNumber1;
    EditText etNumber2;
    TextView result;
    double num1;
    double num2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  
    
    public void addNums(View v) {
        if (etNumber1.getText() != null && etNumber2.getText() != null  ) {
            try{
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(num2 + num1));
            }
            catch (Exception e){
                result.setText(e.getMessage());
            }

            }
    }  

    public void subNums(View v) {
        if (etNumber1.getText() != null && etNumber2.getText() != null  ) {
            try{
                num1 = Double.parseDouble(etNumber1.getText().toString());
                num2 = Double.parseDouble(etNumber2.getText().toString());
                result.setText(Double.toString(num1-num2));
            }
            catch (Exception e){
                result.setText(e.getMessage());
            }
        }
    }

    public void mtpNums(View v) {
        if (etNumber1.getText() != null && etNumber2.getText() != null  ) {
            try{
                num1 = Double.parseDouble(etNumber1.getText().toString());
                num2 = Double.parseDouble(etNumber2.getText().toString());
                result.setText(Double.toString(num2 * num1));
            }
            catch (Exception e){
                result.setText(e.getMessage());
            }
        }
    }

    public void dvnNums(View v) {
        if (etNumber1.getText() != null && etNumber2.getText() != null  ) {

            try{
                 num1 = Double.parseDouble(etNumber1.getText().toString());
                 num2 = Double.parseDouble(etNumber2.getText().toString());
                if (num2 == 0) {
                     result.setText("You cannot divide by zero!!");
                } else {
                    result.setText(Double.toString(num1 / num2));
                 }
            }
            catch (Exception e){
                result.setText(e.getMessage());
            }
        }
    }

    public void clearNums(View v){
        result.setText("");
        etNumber1.setText("");
        etNumber2.setText("");
    }

}